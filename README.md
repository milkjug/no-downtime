# No Downtime

#### a editable python script that alerts you via email if your fav. website is offline

"No Downtime" runs in the background at intervals of 45 minutes to see if your favorite websites down and sends a message if they are.

### Dependencies

* Requests

### Running nodowntime.py

1. Fill in the email and password variables with your email credentials.

2. Change the SMTP server to match your email platform's credentials as well.

2. Add the sites you would like to monitor to the sites list.

3. Copy the script into init.d and set it to run on launch.

`sudo mv nodowntime.py /etc/init.d/`

`sudo +x /etc/init.d/nodowntime.py` 

`sudo update-rc.d nodowntime.py defaults`
